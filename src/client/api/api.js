import axios from 'axios'
import Mock from 'mockjs'

const Random = Mock.Random
//todo
//1. demo
//webMeta
const webMeta = Mock.mock({
    "data|1-50": [
      {
        "id|+1": 1,
        "title": "@title(3,5)",
        "path":"@url()"
      }
    ]
  }
)

const sportEvent = Mock.mock({
  "data":{
      "pages":{
        "current_page":1,
        "total_page":12
     },
     "matches|1-20":[
       {
         "event_time": '@DATETIME("yyyy-MM-dd HH:mm:ss")',
         "league_name": `${Random.ctitle(4,7)}联赛`,
         "match_id|+1":3428462,
         "host_team_name": `${Random.ctitle(4,9)}队`,
         "visit_team_name": `${Random.ctitle(3,9)}队`,
         'bet_zdy': 2.29,
         'bet_kdy': 2.79,
         'bet_hdy': 3.25,
         'bet_ratio_show': 'H',
         'bet_ratio': '0/0.5',
         'bet_zrq': 1.05,
         'bet_krq': 0.81,
         'bet_big_ratio': '2.5',
         'bet_small_ratio': '2.5',
         'bet_zdx': 0.88,
         'bet_kdx': 0.96,
         'bet_zds': 1.96,
         'bet_kds': 1.93,
         'bet_h_zdy': 2.88,
         'bet_h_kdy': 3.3,
         'bet_h_hdy': 2.14,
         'bet_h_ratio_show': 'H',
         'bet_h_ratio': '0',
         'bet_h_zrq': 0.76,
         'bet_h_krq': 1.11,
         'bet_h_big_ratio': '1',
         'bet_h_small_ratio': '1',
         'bet_h_zdx': 0.86,
         'bet_h_kdx': 0.98,
       }
     ]
  }
})

export function fetchWebMeta(cb) {
  setTimeout(() => {
    if (webMeta.data.length > 0) {
      cb(webMeta.data)
    }
    else {
      cb(new Error('no WebMeta'))
    }
  },100)
}

export function fetchSportNav(cb){

}

export function fetchWebController(webAccountId, controllerName) {
  return axios.get('/api/webMeta')
}

export function fetchWebControllerList(webAccountId) {
  return axios.get('/api/webMeta')
}

export function fetchWebHeader(webAccountId, controllerId) {
  return axios.get('/api/webMeta')
}

export function fetchWebDataTemplate(webAccountId, controllerId) {
  return axios.get('/api/webMeta')
}

export function fetchWebMetaById(webAccountId, headerId, tempDataId) {
  return axios.post('/api/webMeta', {id: webId, hId: headerId, tId: tempDataId})
}

export function createWebMeta(hostName, hostUrl, hasCaptcha) {
  return axios.post('/api/webMeta')
}


//sportData
export function fetchSportEventTitle(webAccountId, sportType, time) {
  return sportEvent.data
}

export function fetchSportEvent(webAccountId, sportType, pageNumber, time) {
   return sportEvent.data
}


//web Account info
export function fetchBetLimit(webAcountId) {
}

export function fetchAccountNotice(webAccountId) {
}

export function orderBet(matchId, webAccountId, betAmount, betType, betPoint, userId) {
}

export function getUserBalance(webAccountId, userId) {
}


// user account
export function importUserAccount() {
}

export function getUserList() {
}


export function addUserAccount() {
}

export function updateUserAccount() {
}

export function delUserAccount() {
}

/*
*
*
* */
