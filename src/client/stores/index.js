import Vue from 'vue'
import Vuex from 'vuex'
import sportBoard from './modules/sportBoardStore'


Vue.use(Vuex)


const store = new Vuex.Store(
  {
    strict: process.env.NODE_ENV !== 'production',
    modules: {sportBoard}
  }
)
export default store
