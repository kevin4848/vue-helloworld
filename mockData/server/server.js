const jsonServer = require('json-server')
const path = require('path')
const bodyParser = require('body-parser')
const server = jsonServer.create()
const middlewares = jsonServer.defaults()
const db = require('../mockdb.js')
const router = jsonServer.router(db)
// const routes = require('./routers')
const userAccount = require('../modules/userAccountService.js')
const webMetaService = require('../modules/webMetaService.js')
server.use(bodyParser.json())
server.use(bodyParser.urlencoded({extended: false}))

//const routers = jsonServer.router(path.join(__dirname, '../../mockData/mockdb.json'))
server.get('/getWebMetaList',
  (req, res, next) => {
    let m = webMetaService.getWebMata()
    res.json(m)
  }
)
server.post('/getWebMeta',
  (req, res, next) => {
    let m = webMetaService.getWebMetaById(req.body.id)
    res.json(m)
  }
)
server.post('/getNotice', (req,res,next) => {
  let m = webMetaService.getNoticeList(req.body.id)
  res.json(m)
})

//server.post('/getWebMetaList')
server.post('/getAccountList')
server.post('/orderBet')
server.post('/orderBetList')

server.post('/getAccountInfo')
server.post('/getAccountBalance')

server.use(middlewares)

server.use((request, res, next) => {
  request.method = 'GET';
  // console.log(request)
  next();
})


server.use(router)

server.listen(3000, () => {
  console.log('JSON Server is running')
})
