const Mock = require('mockjs')
const Random = Mock.Random

let web_data = Mock.mock(
  {
    "data|1-20": [
      {
        "id|+1": 1,
        "title": "@title(2,7)",
        "path": "@url()"
      }
    ]
  }
)

let notice = Mock.mock(
  {
    "data|1-50": [
      {
        "nid|+1": 200,
        "msginfo": "@cparagraph(1, 3)",
        "isshow": 1
      }
    ]
  })

let webMetaList = {
  getWebMata: () => {
    return web_data['data']
  },
  getWebMetaById: id => {
    let msg = "no item"
    return web_data['data'].find(item => item.id === id) || {"error": `${msg}`}
  },
  getbryAccountLimit: id => {
    return {"success": true, "betmin": 10, "betmax": 200000, "money": 0.00}
  },
  getNoticeList(id) {
    if (id === undefined) {
      return notice['data']
    }
    else {
      return notice['data'].find(item => item.nid === id)
    }
  }

}

module.exports = webMetaList
