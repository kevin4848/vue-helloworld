const Mock = require('mockjs')
const Random = Mock.Random


let userAccount = {
  countDemo: num => {
    let result = {
      code: 200,
      data: null,
      msg: '获取成功',
      time: new Date()
    }

    result = Mock.mock({
      "data|1-50": [
        {
          "id|+1": num,
          "title": "@title(3,5)"
        }
      ]
    })
    return result;
    //   return {name: ++num}
  }
}


module.exports = userAccount
