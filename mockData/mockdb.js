const Mock = require('mockjs')
const Random = Mock.Random
const sportData = require('./modules/sportEventService').sportData

let webMock = Mock.mock({
    "webMeta|1-50": [{
      "id|+1": 1,
      "title": "@title(3,5)",
      "url": "@url",
      "controller": [
        {"name": "login", "path": "/login", "headerTemp": Random.integer(60, 100), "dataTemp": Random.integer(0, 9)},
        {"name": "captcha", "path": "/valiCode", "headerTemp": Random.integer(60, 100), "dataTemp": Random.integer(0, 9)},
        {
          "name": "sportList",
          "path": "/sportList",
          "headerTemp": Random.integer(60, 100),
          "dataTemp": Random.integer(0, 9)
        },
        {"name": "betOrder", "path": "/addBet", "headerTemp": Random.integer(60, 100), "dataTemp": Random.integer(0, 9)},
        {"name": "getNotice", "path": "/notice", "headerTemp": Random.integer(60, 100), "dataTemp": Random.integer(0, 9)},
        {
          "name": "getBalance",
          "path": "/balance",
          "headerTemp": Random.integer(60, 100),
          "dataTemp": Random.integer(0, 9)
        },
      ]
    }
    ]
  }
)
let data = {
  "employees": [
    {
      "id": 1,
      "first_name": "Sebastian",
      "last_name": "Eschweiler",
      "email": "sebastian@codingthesmartway.com"
    },
    {
      "id": 2,
      "first_name": "Steve",
      "last_name": "Palmer",
      "email": "steve@codingthesmartway.com"
    },
    {
      "id": 4,
      "first_name": "Ann",
      "last_name": "Smith",
      "email": "ann@codingthesmartway.com"
    }
  ]
}

let countNumber = function(num){
  return num++
}

let getBetLimit = Mock.mock({"success": true, "betmin": 10, "betmax": 200000, "money": 0.00})
let getUserMoney = Mock.mock("{\"Error\":0,\"SL\":\"0.00\",\"MC\":\"10\"}")
let getlateOrderList = Mock.mock(({"fy": {"p_page": 10, "page": 1}, "db": "[]"}))
let getNoticeInfo = Mock.mock({
  "fy": {"p_page": 1, "page": 1},
  "db": "[{\"nid\":540,\"msginfo\":\"足球赛事:09月28日 土耳其超级联赛-特别投注 (加拉塔萨雷-角球数 VS 普野社希尔埃尔祖鲁姆士邦-角球数) 第十二个角球赛果已更正为(普野社希尔埃尔祖鲁姆士邦), 请在香港时间 (09月29日) 19:30 过后重新检查您的帐目. 如有不便之处, 敬请见谅.\",\"isshow\":1,\"nclass\":2,\"begintime\":\"2018-09-29T07:50:08\",\"endtime\":\"2018-10-06T07:50:08\",\"sortno\":1,\"newCount\":0},{\"nid\":538,\"msginfo\":\"足球赛事:09月27日 意大利甲组联赛 (恩波利 vs AC米兰) 首个进球方式赛果已更正为(乌龙球), 请在香港时间 (09月29日) 00:10 过后重新检查您的帐目. 如有不便之处, 敬请见谅.\",\"isshow\":1,\"nclass\":2,\"begintime\":\"2018-09-28T12:12:13\",\"endtime\":\"2018-10-05T12:12:13\",\"sortno\":1,\"newCount\":0},{\"nid\":534,\"msginfo\":\"足球赛事:09月26日 亚足联U16锦标赛2018(在马来西亚) (马来西亚U16 VS 日本U16) 赛事已延赛至09月27日开赛, 所有的注单本公司依然视为有效.\",\"isshow\":1,\"nclass\":2,\"begintime\":\"2018-09-26T07:56:30\",\"endtime\":\"2018-10-03T07:56:30\",\"sortno\":1,\"newCount\":0},{\"nid\":533,\"msginfo\":\"足球赛事:09月26日 亚足联U16锦标赛2018(在马来西亚) (泰国U16 VS 塔吉克斯坦U16) 赛事已延赛至09月27日开赛, 所有的注单本公司依然视为有效.\",\"isshow\":1,\"nclass\":2,\"begintime\":\"2018-09-26T07:56:30\",\"endtime\":\"2018-10-03T07:56:30\",\"sortno\":1,\"newCount\":0},{\"nid\":529,\"msginfo\":\"足球赛事:09月26日 亚足联U16锦标赛2018(在马来西亚) (泰国U16 VS 塔吉克斯坦U16) 赛事已延赛至09月26日开赛, 所有的注单本公司依然视为有效.\",\"isshow\":1,\"nclass\":2,\"begintime\":\"2018-09-26T07:50:53\",\"endtime\":\"2018-10-03T07:50:53\",\"sortno\":1,\"newCount\":0},{\"nid\":528,\"msginfo\":\"足球赛事:09月26日 亚足联U16锦标赛2018(在马来西亚) (马来西亚U16 VS 日本U16) 赛事已延赛至09月26日开赛, 所有的注单本公司依然视为有效.\",\"isshow\":1,\"nclass\":2,\"begintime\":\"2018-09-26T07:41:05\",\"endtime\":\"2018-10-03T07:41:05\",\"sortno\":1,\"newCount\":0},{\"nid\":527,\"msginfo\":\"棒球赛事:09月25日 美国职业棒球 (波士顿红袜队 VS 巴尔的摩金莺队) 赛事已延赛至09月26日开赛, 所有的注单本公司依然视为有效.\",\"isshow\":1,\"nclass\":2,\"begintime\":\"2018-09-26T01:30:15\",\"endtime\":\"2018-10-03T01:30:15\",\"sortno\":1,\"newCount\":0},{\"nid\":526,\"msginfo\":\"棒球赛事:09月25日 美国职业棒球 (波士顿红袜队 VS 巴尔的摩金莺队) 赛事已延赛至09月26日 13:10开赛, 所有的注单本公司依然视为\",\"isshow\":1,\"nclass\":2,\"begintime\":\"2018-09-25T23:27:13\",\"endtime\":\"2018-10-02T23:27:13\",\"sortno\":1,\"newCount\":0},{\"nid\":525,\"msginfo\":\"足球赛事:09月25日 爱尔兰超级联赛 (登克尔克 VS 德利城) 因赛事延迟至15:00开赛, 所有提前投注在滚球的注单本公司一律视为有效.\",\"isshow\":1,\"nclass\":2,\"begintime\":\"2018-09-25T15:05:51\",\"endtime\":\"2018-10-02T15:05:51\",\"sortno\":1,\"newCount\":0},{\"nid\":524,\"msginfo\":\"尊贵的客户们！值此中秋团圆佳节来临之际，祝愿佳节事业顺利步步高，阖家幸福永平安，中秋节快乐！\",\"isshow\":1,\"nclass\":2,\"begintime\":\"2018-09-23T01:50:32\",\"endtime\":\"2018-09-30T01:50:32\",\"sortno\":1,\"newCount\":0}]"
})

module.exports = {
  demos: data.employees,
  listData: sportData,
  eventData: sportData.db,
  webMeta: webMock.webMeta,
}
